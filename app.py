# app.py - Python Flask application

from flask import Flask, render_template

app = Flask(__name__)

# Define some sample courses
courses = [
    {"id": 1, "title": "- Python Programming", "description": "Learn Python programming language by following these  steps:"},
    {"id": 2, "title": "- Web Development", "description": "Learn web development using HTML, CSS, and JavaScript."},
    {"id": 3, "title": "- Data Science", "description": "Learn data science and analysis with Python."},
    {"id": 4, "title": "- Devops", "description": "Learn devops."},
    {"id": 5, "title": "- CyberSecurity", "description": "Learn CyberSecurity."},
    {"id": 6, "title": "- Business Intelligence", "description": "Learn Business Intelligence."},
    {"id": 7, "title": "- Cloud Computing", "description": "Learn Cloud Computing."},
    {"id": 8, "title": "- IA & Machine Learning", "description": "Learn IA & Machine Learning."},
    {"id": 9, "title": "- VR & Gaming", "description": "Learn VR & Gaming."},

]

@app.route('/')
def index():
    return render_template('index.html', courses=courses)

@app.route('/course/<int:course_id>')
def course(course_id):
    course = None
    for c in courses:
        if c['id'] == course_id:
            course = c
            break
    return render_template('course.html', course=course)

if __name__ == '__main__':
    app.run(debug=True)

app = Flask(__name__, static_url_path='/static')
